{-# LANGUAGE OverloadedStrings #-}
module Lib where

import Text.Parsec.Error
import Text.CSV
import Data.List
-- import qualified Data.Text as T

thesaurusLookup :: FilePath -> String -> IO (Either ParseError (Maybe Record))
thesaurusLookup fp w = do
  tryparse <- parseCSVFromFile fp
  case tryparse of
    Left e -> return $ Left e
    Right c -> return $ Right $ lookupWord c w

parseThesaurus :: FilePath -> IO (Either ParseError CSV)
parseThesaurus = parseCSVFromFile

lookupWord :: CSV -> String -> Maybe Record
lookupWord rs w = do
  let rs' = filter (\r -> head r == w) rs
  if not (null  rs')
    then Just $ head rs'
    else Nothing

justIsograms :: Record -> Record
justIsograms = filter isIsogram

isIsogram :: String -> Bool
isIsogram t = not $ any (\letters -> length letters /= 1) $ group $ sort t


opportuneIsogram :: CSV -> Int -> String -> Either String Record
opportuneIsogram a p t = do
              -- ^ see what I did there?..."apt" HA!
  let r = lookupWord a t
  case r of
    Nothing -> Left $ t ++ " not found"
    Just rs -> Right $ filter (\i -> length i == p) (justIsograms rs)
