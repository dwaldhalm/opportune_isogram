# Why do this in the first place?
Because you should not fall victim to one of the classic blunders - the most famous of which is
"Do not repeat yourself" - but only slightly less well-known is this:
"Never miss an opportunity to leave a snarky easter egg in your code!"

