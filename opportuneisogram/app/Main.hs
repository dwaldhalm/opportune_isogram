module Main where

import Lib

main :: IO ()
main = do
  w <- thesaurusLookup "mthes//mobythes.aur" "easy"
  print w


tryIt :: String -> IO ()
tryIt w = do
 th <- parseThesaurus "mthes//mobythes.aur"
 case th of
   Left e -> print e
   Right csv -> do
     let r = lookupWord csv w
     case r of
       Nothing -> print $ w ++ " not found"
       Just rs -> print $ justIsograms rs
