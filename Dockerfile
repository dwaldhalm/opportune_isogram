# Dockerfile for Opportune Isogram
# https://bitbucket.org/dwaldhalm/opportune_isogram

FROM haskell:8

MAINTAINER Don Waldhalm <don.waldhalm@gmail.com>

# build tools
RUN cabal update
RUN stack upgrade

# make a snap app with stack--for the stack dependencies
WORKDIR /opt/oiweb
RUN cabal install snap-templates
RUN snap init default
COPY ./oiweb/stack.yaml /opt/oiweb/stack.yaml
RUN sed -i "s|- '../opportuneisogram/'||" /opt/oiweb/stack.yaml
RUN stack setup
RUN stack build

# now, we should have a bootstrapped snap app on stack
# so we'll only need dependencies for our project
# add and install the app code (we're ignoring .stack-work)
COPY ./opportuneisogram /opt/opportuneisogram
COPY ./oiweb /opt/oiweb
RUN stack install

# runs the installed oiweb app on port 80
EXPOSE 80
CMD oiweb -p 80