<html>
  <head>
    <title>Opportune Isogram</title>
    <link rel="stylesheet" href="http://yegor256.github.io/tacit/tacit.min.css"/>
  </head>
  <body>
    <a href="/">Home</a>
    <a href="/isogram">Isogram</a>
    <hr/>
    <div id="content">
      <apply-content/>
    </div>
  </body>
</html>
