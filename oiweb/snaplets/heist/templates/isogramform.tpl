<form method="post" action="${postAction}">
  <fieldset>
    <label for="word">Word:</label>
    <input type="text" name="word" size="20" value="${word}" />

    <label for="length">Length:</label>
    <input type="number" name="length" size="2" value="${length}" />

    <input type="submit" value="${submitText}" />
  </fieldset>
</form>
