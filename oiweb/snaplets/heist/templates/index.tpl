<apply template="base">

<h2> What </h2>
<p>
Given a word and length, find isogram-synonyms of the given length.
</p>

<h2> But Why? </h2>
<p>
To avoid programming blunders.
</p>
<blockquote>
  ...the most famous of which is "do not repeat yourself" (DRY), but only slightly less well-known is
  "never miss the opportunity to lay an Easter egg!"
</blockquote>
<img src="/blunders.jpg" alt="blunders"/>

<p>
Haskell offers us frequent opportunities to leave our mark on unsuspecting library authors, with its strings of single-letter variables.
The only limitation is that we can't use the same letter twice.
Naturally, we can split words into chunks, or use entire words for variable names,
but both techniques come across as desperate and clumsy.
It can be hard to think of a seven-letter isogram that means "simple" when you're dealing with a seven-variable function.
And nothing is worse than appearing clumsy when you're trying to be clever.
</p>

<blockquote>
...I know words--I have the best words--I have the best!
</blockquote>
<img src="/bestwords1.jpg" alt="bestwords"/>

</apply>
