{-# LANGUAGE OverloadedStrings #-}

------------------------------------------------------------------------------
-- | This module is where all the routes and handlers are defined for your
-- site. The 'app' function is the initializer that combines everything
-- together and is exported by this module.
module Site
  ( app
  ) where

------------------------------------------------------------------------------
import           Control.Applicative
import           Data.ByteString (ByteString)
import           Data.Map.Syntax ((##))
import qualified Data.Text as T
import           Snap.Core
import           Snap.Snaplet
import           Snap.Snaplet.Heist
import           Snap.Util.FileServe
import qualified Heist.Interpreted as I
import qualified Heist as H
------------------------------------------------------------------------------
import qualified Data.ByteString.Char8 as BS
------------------------------------------------------------------------------
import           Lib (parseThesaurus, opportuneIsogram)
import           Control.Monad.IO.Class
import           Data.List (intersperse)
import           Data.IORef
import           Control.Monad.State (gets)
import           Application

------------------------------------------------------------------------------
-- | Render isogram form
handleIsogram :: Handler App App ()
handleIsogram = method GET handleForm <|> method POST handleFormSubmit
  where handleForm = do
          let ir = IsogramRequest "agonize" 4
          renderWithSplices "isogram" (isogramRequestSplice ir [])
        handleFormSubmit = do
          Just w <- getPostParam "word"
          Just l <- getPostParam "length"
          submitisogram (T.pack $ BS.unpack w) (T.pack $ BS.unpack l)

-- | Handle the isogram generation request
submitisogram :: T.Text -> T.Text -> Handler App App ()
submitisogram w l = do
  let wl = read $ T.unpack l :: Int
  thesaurusRef <- gets _thesaurus
  csv <-  liftIO $ readIORef thesaurusRef
  let ois = opportuneIsogram csv wl (T.unpack w)
  let ir = IsogramRequest w wl
  case ois of
    Right ws ->
      if not (null ws)
        then renderWithSplices "isogram" (isogramRequestSplice ir ws)
        else renderWithSplices "isogram"
              (isogramRequestSplice ir ["sorry, none of that length"])
    Left e -> renderWithSplices "isogram" (isogramRequestSplice ir [e])


-- | a type to represent an isogram request
data IsogramRequest =
  IsogramRequest { irWord :: T.Text
                 , irNumber :: Int
                 } deriving (Eq, Show, Read)

-- | how we map Haskell to a template
isogramRequestSplice :: Monad n
                     => IsogramRequest -> [String] -> H.Splices (I.Splice n)
isogramRequestSplice ir rs = do
  "word" ## I.textSplice (irWord ir)
  "length" ## I.textSplice (T.pack $ show $ irNumber ir)
  "postAction" ## I.textSplice "isogram"
  "submitText" ## I.textSplice "generate isogram"
  "wordlist" ## I.textSplice $ T.pack $
    foldr (\ a b -> b `mappend`  (" " ++ a)) mempty (intersperse ", " rs)

------------------------------------------------------------------------------
-- | The application's routes.
routes :: [(ByteString, Handler App App ())]
routes = [ ("isogram",  handleIsogram)
         , ("",         serveDirectory "static")
         ]


------------------------------------------------------------------------------
-- | The application initializer.
app :: SnapletInit App App
app = makeSnaplet "app" "Opportune Isograms" Nothing $ do
    h <- nestSnaplet "" heist $ heistInit "templates"
    Right th <- liftIO $
      parseThesaurus "../opportuneisogram//mthes//mobythes.aur"
    ref <- liftIO $ newIORef th
    addRoutes routes
    return $ App h ref

