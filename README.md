# My project's README

I've improved the build time for the docker image by "bootstrapping" to a default snap app.
I've copied my stack.yaml to include dependencies, but removed (with sed) the line that calls for the opportune-isogram library.

I can build the latest from local source like this:
```docker build -t dwaldhalm/opportune-isogram```
